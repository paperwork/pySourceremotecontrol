import serial
import sys

import pySourceremotecontrol.sources_general as src

import logging

LOG = logging.getLogger("pySourceremotecontrol.sources_toe895x")

class TOEError(src.PowerSourceError):
    pass

class UnsupportedBaudrateError(TOEError):
    pass

class UnsupportedStopbitsError(TOEError):
    pass

class UnsupportedHandshake(TOEError):
    pass

class UnsupportedSource(TOEError):
    pass

class NoConnectionError(TOEError):
    pass

class InvalidOutputError(TOEError):
    pass

class toe895x(src.PowerSource):
    HANDSHAKE_XONXOFF   = 0
    HANDSHAKE_RTS       = 1

    def __init__(self, comport, baudrate=57600, stopbits=1, parity='N',
                 handshake=HANDSHAKE_XONXOFF):
        src.PowerSource.__init__(self)
        self._serial = serial.Serial(timeout=1)
        self._serial.port = comport
        self._devinfo = src.PowerSourceDeviceInformation()
        
        supportedBaudrates = [1200, 2400, 4800, 9600, 19200, 38400, 57600]
        if not baudrate in supportedBaudrates:
            raise UnsupportedBaudrateError
        self._serial.baudrate = baudrate

        if not stopbits in [1,2]:
            raise UnsupportedStopbitsError
        self._serial.stopbits = stopbits

        self._serial.parity = parity
        self._serial.bytesize = 8
        
        supportedHandshake = [0,1]
        if not handshake in supportedHandshake:
            raise UnsupportedHandshake
        if handshake == toe895x.HANDSHAKE_XONXOFF:
            self._serial.xonxoff = 1
            self._serial.rtscts  = 0
        else:
            self._serial.rtscts  = 1
            self._serial.xonxoff = 0

        self._lastVoltages = [0, 0]
        self._lastOutput = 0 # 0 is invalid

    def connect(self):
        if self._serial.isOpen():
            self._serial.close()

        LOG.debug("Connecting to TOE895x on port %s with baudrate %d", self._serial.port, self._serial.baudrate)

        try:
            self._serial.open()
        except Exception as e:
            LOG.error("--- ERROR opening serial port: %s ---", e)
            self._serial.close()
        else:
            str = self.TOESendRecv("*IDN?")
            LOG.debug("*IDN: %s", str)

            if "TOE895" in str:
                self.TOESendCmd("SYST:REM")
                self.TOEClearErrorLog()
                
                # switch to SCPI if needed
                s = self.TOESendRecv("SYST:LANG?")
                if "COMP" in s:
                    TOESendCmd("SYST:LANG CIIL")
                # get device information
                self.TOEGetDeviceInformation()

                LOG.debug(self._devinfo)

                self._lastVoltages[0] = self.getVoltage(1)
                if self._devinfo.outputs > 1:
                    self._lastVoltages[1] = self.getVoltage(2)

            else:
                self._serial.close()
                raise UnsupportedSource

    def disconnect(self):
        if self._serial.isOpen():
            LOG.debug("Disconnecting from TOE895x")

            self.TOEClearErrorLog()
            lastErr = 0
            counter = 5
            while (counter > 0) and (lastErr != -201):
                self.TOESendCmd("SYST:LOC")
                self.TOESendCmd("DISP 0")
                lastErr = self.TOEGetLastError()
                counter -= 1
            self._serial.close()
        return 0

    def getMinimalUpdateTimeMs(self):
        return 50

    def getVoltage(self, output):
        self.checkLastOutput(output)
        s = self.TOESendRecv("VOLT?")
        try:
            f = float(s)
        except ValueError:
            f = 0.0
        return f

    def setVoltage(self, output, voltage):
        self.checkLastOutput(output)
        s = "VOLT %.2f" % voltage
        if voltage > 0:
            self._lastVoltages[(output-1)] = voltage
        self.TOESendCmd(s)

    def isOutputEnabled(self, output):
        retval = False
        if output == -1:
            s = self.TOESendRecv("OUTP?")
            if s[0] == '1':
                retval = True
        return retval 

    def enableOutput(self, output, value):
        if output == -1:
            self.enableOutputs(value)
        else:
            self.checkLastOutput(output)
            if value:
                self.setVoltage(output, self._lastVoltages[(output-1)])
            else:
                self._lastVoltages[(output-1)] = self.getVoltage(output)
                self.setVoltage(output, 0)

    def enableOutputs(self, value):
        if value:
            self.TOESendCmd("OUTP 1")
        else:
            self.TOESendCmd("OUTP 0")

    def getDeviceInformation(self):
        return self._devinfo

    # internal functions 
    def checkLastOutput(self, output):
        if output > self._devinfo.outputs or output < 1:
            raise InvalidOutputError
        if self._lastOutput != output:
            self.TOESendCmd("INST OUT" + str(output))
            self._lastOutput = output
                

    def TOESendCmd(self, command):
        if not self._serial.isOpen():
            raise NoConnectionError
        command += "\r\n"
        command = command.encode()
        self._serial.write(command)

    def TOESendRecv(self, command):
        self.TOESendCmd(command)
        try:
#            s = self._serial.readline(size=None, eol='\n')
            s = self._serial.readline().decode()
        except serial.SerialTimeoutException:
            s = "Timeout waiting for device";
        return s

    def TOEGetLastError(self):
        if not self._serial.isOpen():
            raise NoConnectionError
        s = self.TOESendRecv("SYST:ERR?")
        tmp = s.split(',');
        try:
            retval = int(tmp[0])
        except ValueError:
            retval = 0
        return retval

    def TOEClearErrorLog(self):
        counter = 20 # maximum error count storeable in device
        while (self.TOEGetLastError() != 0) and (counter > 0):
            counter -= 1

    def TOEGetDeviceInformation(self):
        s = self.TOESendRecv("*IDN?")
        sList = s.split(',')

        self._devinfo.producer = sList[0].strip()
        self._devinfo.productName = sList[1].strip()
        self._devinfo.serialNo = sList[2].strip()
        self._devinfo.softwareRevision = sList[3].strip()

        s = self.TOESendRecv("CURR? MIN")
        self._devinfo.currentMin = float(s)
        s = self.TOESendRecv("CURR? MAX")
        self._devinfo.currentMax = float(s)
        s = self.TOESendRecv("CURR:LIM? MIN")
        self._devinfo.currentLimitMin = float(s)
        s = self.TOESendRecv("CURR:LIM? MAX")
        self._devinfo.currentLimitMax = float(s)
        s = self.TOESendRecv("VOLT? MIN")
        self._devinfo.voltageMin = float(s)
        s = self.TOESendRecv("VOLT? MAX")
        self._devinfo.voltageMax = float(s)
        s = self.TOESendRecv("VOLT:LIM? MIN")
        self._devinfo.voltageLimitMin = float(s)
        s = self.TOESendRecv("VOLT:LIM? MAX")
        self._devinfo.voltageLimitMax = float(s)
        s = self.TOESendRecv("VOLT:PROT? MIN")
        self._devinfo.voltageOVPMin = float(s)
        s = self.TOESendRecv("VOLT:PROT? MAX")
        self._devinfo.voltageOVPMax = float(s)
        
        if "8952" in self._devinfo.productName:
            self._devinfo.outputs = 2
        else:
            self._devinfo.outputs = 1

        self._devinfo.singleTurnOnOff = False

