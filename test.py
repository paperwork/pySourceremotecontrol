import sys

sys.path = [".."] + sys.path

import pySourceremotecontrol.function_iec61131_ramps as ir
import pySourceremotecontrol.sources_test as ts
import pySourceremotecontrol.sources_toe895x as toe

def runCheck(checkToRun):
    retval = 0
    if checkToRun == 0:
        print("Check to be online")
    else:
        print("Check for being offline")
    return retval

t = ts.TestSource()
t = toe.toe895x("/dev/ttyS0")
t.connect()
t.setVoltage(1,24.00)
t.setVoltage(2,22.0)
ramp = ir.Ramp611312(source=t, checkCallback=runCheck, output=1, voltageStart=23, runChecks=True, count=1)
ramp.run()
t.disconnect()

