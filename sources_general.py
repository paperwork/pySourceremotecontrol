class PowerSourceError(Exception):
    pass

class PowerSourceNotImplemented(PowerSourceError):
    pass

class PowerSource(object):

    def __init__(self):
        pass

    def connect(self):
        raise PowerSourceNotImplemented

    def disconnect(self):
        raise PowerSourceNotImplemented

    def getMinimalUpdateTimeMs(self):
        raise PowerSourceNotImplemented

    def getVoltage(self, output):
        raise PowerSourceNotImplemented

    def setVoltage(self, output, voltage):
        raise PowerSourceNotImplemented

    def isOutputEnabled(self, output):
        raise PowerSourceNotImplemented

    def enableOutput(self, output, value):
        raise PowerSourceNotImplemented
    
    def enableOutputs(self, value):
        raise PowerSourceNotImplemented

    def getDeviceInformation(self):
        raise PowerSourceNotImplemented

class PowerSourceDeviceInformation(object):
    def __init__(self):
        self.producer = "Not defined"
        self.productName = "No product"
        self.serialNo = "0"
        self.softwareRevision = "0"
        self.outputs = 0
        self.currentMin = 0.0
        self.currentMax = 0.0
        self.currentLimitMin = 0.0
        self.currentLimitMax = 0.0
        self.voltageMin = 0.0
        self.voltageMax = 0.0
        self.voltageLimitMin = 0.0
        self.voltageLimitMax = 0.0
        self.voltageOVPMin = 0.0
        self.voltageOVPMax = 0.0
        self.singleTurnOnOff = False

    def dump(self):
        print(self)

    def __str__(self):
        s = ""
        s += "Producer           {0}\n".format(self.producer)
        s += "Product name       {0}\n".format(self.productName)
        s += "Serial number      {0}\n".format(self.serialNo)
        s += "Software Revision  {0}\n".format(self.softwareRevision)

        s += "No of outputs      {0}\n".format(self.outputs)

        s += "Current min        {0}\n".format(self.currentMin)
        s += "Current max        {0}\n".format(self.currentMax)
        s += "Current limit min  {0}\n".format(self.currentLimitMin)
        s += "Current limit max  {0}\n".format(self.currentLimitMax)
        s += "Voltage min        {0}\n".format(self.voltageMin)
        s += "Voltage max        {0}\n".format(self.voltageMax)
        s += "Voltage limit min  {0}\n".format(self.voltageLimitMin)
        s += "Voltage limit max  {0}\n".format(self.voltageLimitMax)
        s += "Voltage OVP min    {0}\n".format(self.voltageOVPMin)
        s += "Voltage OVP max    {0}\n".format(self.voltageOVPMax)

        s += "Single Turn On/Off {0}\n".format(self.singleTurnOnOff)

        return s

