import serial
import sys

import pySourceremotecontrol.sources_general as src

import logging

LOG = logging.getLogger("pySourceremotecontrol.sources_dp800")

class DP800(src.PowerSource):
	"""
	Rigol DP800 power supply. Requires the VISA libraries to be installed.
	"""
	def __init__(self, rsrc=None):
		self._rsrc = rsrc
		self._io = None

		self._devinfo = src.PowerSourceDeviceInformation()

		self._last_voltages = [0, 0, 0]
		self._last_output = 0 # 0 is invalid

	def _write(self, cmd):
		import time

		self._io.write(cmd)

	def connect(self):
		self.disconnect()

		import pyvisa.visa

		rsrc = self._rsrc
		if rsrc is None:
			try:
				rsrc = pyvisa.vpp43.find_resources(pyvisa.visa.resource_manager.session, "USB?::0x1AB1::0x0E11?*")[2].decode("ASCII")
			except pyvisa.visa_exceptions.VisaIOError as e:
				raise Exception("No DP800 source found") from e

		self._io = pyvisa.visa.Instrument(rsrc, timeout=100)

		self._write("*IDN?")
		idn = self._io.read()
		if not "DP8" in idn:
			raise Exception("The device \"{}\" is no DP800".format(idn))

		self._write("SYST:REM")
		self._write("DISP:MODE WAVE")
		self._clear_error_log()
		self._read_device_information()

		self._last_output = 0 # 0 is invalid
		self._last_voltages[0] = self.getVoltage(1)
		self._last_voltages[1] = self.getVoltage(2)
		self._last_voltages[2] = self.getVoltage(3)

	def disconnect(self):
		if self._io is None:
			return

		try:
			self._clear_error_log()
			self.write("SYST:LOC")
		except:
			# We don't care
			pass

		try:
			self._io.close()
			pass
		finally:
			self._io = None

		# DON'T REMOVE! Without this, the DP832 will lose its USB
		# connection until power-cycled - I kid you not
		time.sleep(0.2)

	def _clear_error_log(self):
		while self._get_last_error() != 0:
			pass

	def _get_last_error(self):
		self._write("SYST:ERR?")
		result = self._io.read()

		return int(result.split(",")[0])

	def _check_last_output(self, output):
		import time

		assert output >= 1 and output <= 3
		assert not self._io is None

		if self._last_output != 0 and output == self._last_output:
			return

		self._last_output = output

		self._write("INST CH{}".format(output))
		time.sleep(0.050)

	def _read_value(self, query):
		self._write(query)
		return float(self._io.read())

	def getVoltage(self, output):
		self._check_last_output(output)

		v = self._read_value("VOLT?")
		LOG.debug("Read voltage: %f", v)
		return v

	def setVoltage(self, output, voltage):
		self._check_last_output(output)

		if voltage > 0:
			self._last_voltages[output - 1] = voltage

		self._write("VOLT {:.2f}".format(voltage))

	def _restore_last_output(self):
		output = self._last_output
		if output != 0:
			self._last_output = 0
			self._check_last_output(output)

	def enableOutput(self, output, value):
		if output == -1:
			self.enableOutputs(value)
		else:
			self._check_last_output(output)

			if value:
				self.setVoltage(output, self._last_voltages[output - 1])
			else:
				self._last_voltages[output - 1] = self.getVoltage(output)
				self.setVoltage(output, 0)

	def enableOutputs(self, value):
		if value:
			self._write("OUTP CH1,ON")
			self._write("OUTP CH2,ON")
			self._write("OUTP CH3,ON")
		else:
			self._write("OUTP CH1,OFF")
			self._write("OUTP CH2,OFF")
			self._write("OUTP CH3,OFF")

		self._restore_last_output()

	def isOutputEnabled(self, output):
		if output == -1:
			self._write("OUTP? CH1")
			ch1 = self._io.read().strip() == "ON"
			self._write("OUTP? CH2")
			ch2 = self._io.read().strip() == "ON"
			self._write("OUTP? CH3")
			ch3 = self._io.read().strip() == "ON"

			self._restore_last_output()

			return ch1 or ch2 or ch3
		else:
			self._check_last_output(output)

			self._write("OUTP?")
			return self._io.read().strip() != "OFF"

	# -- Device info --
	def getDeviceInformation(self):
		return self._devinfo

	def _read_device_information(self):
		self._write("*IDN?")
		s = self._io.read()

		sList = s.split(',')

		self._devinfo.producer = sList[0].strip()
		self._devinfo.productNAme = sList[1].strip()
		self._devinfo.serialNo = sList[2].strip()
		self._devinfo.softwareRevision = sList[3].strip()

		self._devinfo.currentLimitMin = self._devinfo.currentMin = self._read_value("CURR? MIN")
		self._devinfo.currentLimitMax = self._devinfo.currentMax = self._read_value("CURR? MAX")
		self._devinfo.voltageLimitMin = self._devinfo.voltageMin = self._read_value("VOLT? MIN")
		self._devinfo.voltageLimitMax = self._devinfo.voltageMax = self._read_value("VOLT? MAX")
		self._devinfo.voltageOVPMin = self._read_value("VOLT:PROT? MIN")
		self._devinfo.voltageOVPMax = self._read_value("VOLT:PROT? MAX")

		# We have three outputs, but output 3 is limited
		self._devinfo.outputs = 2

