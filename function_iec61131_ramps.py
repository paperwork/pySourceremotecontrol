import sys

import pySourceremotecontrol.function_general as functions

import logging

LOG = logging.getLogger("pySourceremotecontrol.function_iec61131_ramps")

class Ramp611312(functions.Function):
    RAMPS_ALL          = 0
    RAMPS_6_4_2_1      = 1
    RAMPS_6_4_2_2_FAST = 2
    RAMPS_6_4_2_2_SLOW = 3

    CALLBACK_PARA_DEVICE_ON  = 0
    CALLBACK_PARA_DEVICE_OFF = 1

    def __init__(self, source=None, checkCallback=None, 
                 voltageStart=0.0,
                 voltageSDLav=19.2,
                 output=1,
                 runChecks = True,
                 checkFailedStopRun=True,
                 checkSDLavWaitTimeout=True,
                 timeBetweenTests=5,
                 testToRun=RAMPS_ALL,
                 count=3
                ):
        functions.Function.__init__(self, source, checkCallback)
        self._voltageStart = voltageStart
        self._voltageSDLav = voltageSDLav
        self._output = output
        self._runChecks = runChecks
        self._checkFailedStopRun = checkFailedStopRun
        self._checkSDLavWaitTimeout = checkSDLavWaitTimeout
        self._timeBetweenTests = timeBetweenTests
        self._count = count
        self._testToRun = testToRun

    def run(self):
        import time
        result = 0
        rampCounter = 0
        stopRamp = False
        vBefore = self._source.getVoltage(self._output)

        if self._testToRun == Ramp611312.RAMPS_ALL or self._testToRun == Ramp611312.RAMPS_6_4_2_1:
            cycles = self._count
            vStart = self._voltageStart
            vStop = 0.0
            tStep = self._source.getMinimalUpdateTimeMs() / 1000
            vStep = vStart / (60 / tStep)
            while cycles > 0 and stopRamp==False:
                cycles-=1
                rampCounter+=1
                LOG.info("Run ramp: " + str(rampCounter))

                f = self._voltageStart
                self._source.setVoltage(self._output, f)

                time.sleep(self._timeBetweenTests/2)

                if stopRamp == False and self._runChecks:
                    result = self._checkCallback(Ramp611312.CALLBACK_PARA_DEVICE_ON)
                    if result != 0 and self._checkFailedStopRun and stopRamp == False:
                        LOG.info("IEC 61131-2 6.4.2.1: check before ramp failed")
                        result = 1
                        stopRamp = True

                while (abs(f - vStop)>(vStep - 0.0000002)) and stopRamp == False:
                    self._source.setVoltage(self._output, f)
                    if vStart < vStop:
                        f += vStep
                    else:
                        f -= vStep
                    time.sleep(tStep)

                f = vStop
                self._source.setVoltage(self._output, f)
                
                time.sleep(10)
                # check device is off
                if stopRamp == False and self._runChecks:
                    result = self._checkCallback(Ramp611312.CALLBACK_PARA_DEVICE_OFF)
                    if result != 0 and self._checkFailedStopRun and stopRamp == False:
                        LOG.warning("IEC 61131-2 6.4.2.1: check shutdown device failed")
                        result = 2
                        stopRamp = True

                while (abs(f - vStart)>(vStep - 0.0000002)) and stopRamp == False:
                    self._source.setVoltage(self._output, f)
                    if vStart < vStop:
                        f -= vStep
                    else:
                        f += vStep
                    time.sleep(tStep)

                f = vStart
                self._source.setVoltage(self._output, f)

                time.sleep(self._timeBetweenTests/2)
                # check device is on again
                if stopRamp == False and self._runChecks:
                    result = self._checkCallback(Ramp611312.CALLBACK_PARA_DEVICE_ON)
                    if result != 0 and self._checkFailedStopRun and stopRamp == False:
                        LOG.warning("IEC 61131-2 6.4.2.1: check device is on after ramp failed")
                        result = 3
                        stopRamp = True

        if self._testToRun == Ramp611312.RAMPS_ALL or self._testToRun == Ramp611312.RAMPS_6_4_2_2_FAST:
            cycles = self._count
            vStart = self._voltageStart
            vStop = 0.0
            tStep = self._source.getMinimalUpdateTimeMs() / 1000
            vStep = vStart / (5 / tStep)
            while cycles > 0 and stopRamp==False:
                cycles-=1
                rampCounter+=1
                LOG.info("Run ramp: " + str(rampCounter))

                f = self._voltageStart
                self._source.setVoltage(self._output, f)

                time.sleep(self._timeBetweenTests/2)

                if stopRamp == False and self._runChecks:
                    result = self._checkCallback(Ramp611312.CALLBACK_PARA_DEVICE_ON)
                    if result != 0 and self._checkFailedStopRun and stopRamp == False:
                        LOG.warning("IEC 61131-2 6.4.2.2 fast: check before ramp failed")
                        result = 4
                        stopRamp = True

                while (abs(f - vStop)>(vStep - 0.0000002)) and stopRamp == False:
                    self._source.setVoltage(self._output, f)
                    if vStart < vStop:
                        f += vStep
                    else:
                        f -= vStep
                    time.sleep(tStep)

                f = vStop
                self._source.setVoltage(self._output, f)
                
                time.sleep(tStep)

                # check device is off
                if stopRamp == False and self._runChecks:
                    result = self._checkCallback(Ramp611312.CALLBACK_PARA_DEVICE_OFF)
                    if result != 0 and self._checkFailedStopRun and stopRamp == False:
                        LOG.warning("IEC 61131-2 6.4.2.2 fast: check shutdown device failed")
                        result = 5
                        stopRamp = True

                while (abs(f - vStart)>(vStep - 0.0000002)) and stopRamp == False:
                    self._source.setVoltage(self._output, f)
                    if vStart < vStop:
                        f -= vStep
                    else:
                        f += vStep
                    time.sleep(tStep)

                f = vStart
                self._source.setVoltage(self._output, f)

                time.sleep(self._timeBetweenTests/2)
                # check device is on again
                if stopRamp == False and self._runChecks:
                    result = self._checkCallback(Ramp611312.CALLBACK_PARA_DEVICE_ON)
                    if result != 0 and self._checkFailedStopRun and stopRamp == False:
                        LOG.warning("IEC 61131-2 6.4.2.2 fast: check device is on after ramp failed")
                        result = 6
                        stopRamp = True

        if self._testToRun == Ramp611312.RAMPS_ALL or self._testToRun == Ramp611312.RAMPS_6_4_2_2_SLOW:
            cycles = self._count
            vStart = self._voltageStart
            vStop = self._voltageSDLav
            tStep = self._source.getMinimalUpdateTimeMs() / 1000
            vStep = vStart / (60 / tStep)
            while cycles > 0 and stopRamp==False:
                cycles-=1
                rampCounter+=1
                LOG.info("Run ramp: " + str(rampCounter))

                f = self._voltageStart
                self._source.setVoltage(self._output, f)

                time.sleep(self._timeBetweenTests/2)

                if stopRamp == False and self._runChecks:
                    result = self._checkCallback(Ramp611312.CALLBACK_PARA_DEVICE_ON)
                    if result != 0 and self._checkFailedStopRun and stopRamp == False:
                        LOG.warning("IEC 61131-2 6.4.2.2 slow: check before ramp failed")
                        result = 7
                        stopRamp = True

                while (abs(f - vStop)>(vStep - 0.0000002)) and stopRamp == False:
                    self._source.setVoltage(self._output, f)
                    if vStart < vStop:
                        f += vStep
                    else:
                        f -= vStep
                    time.sleep(tStep)

                f = vStop
                self._source.setVoltage(self._output, f)
                
                time.sleep(tStep)

                # check device is off
                if stopRamp == False and self._runChecks:
                    result = self._checkCallback(Ramp611312.CALLBACK_PARA_DEVICE_OFF)
                    if result != 0 and self._checkFailedStopRun and stopRamp == False:
                        LOG.warning("IEC 61131-2 6.4.2.2 slow: check shutdown device failed")
                        result = 8
                        stopRamp = True

                while (abs(f - vStart)>(vStep - 0.0000002)) and stopRamp == False:
                    self._source.setVoltage(self._output, f)
                    if vStart < vStop:
                        f -= vStep
                    else:
                        f += vStep
                    time.sleep(tStep)

                f = vStart
                self._source.setVoltage(self._output, f)

                time.sleep(self._timeBetweenTests/2)
                # check device is on again
                if stopRamp == False and self._runChecks:
                    result = self._checkCallback(Ramp611312.CALLBACK_PARA_DEVICE_ON)
                    if result != 0 and self._checkFailedStopRun and stopRamp == False:
                        LOG.warning("IEC 61131-2 6.4.2.2 slow: check device is on after ramp failed")
                        result = 9
                        stopRamp = True

        self._source.setVoltage(self._output, vBefore)
        return result

