import serial
import sys

import pySourceremotecontrol.sources_general as src

import logging

LOG = logging.getLogger("pySourceremotecontrol.sources_kp3005p")

class KP3005PError(src.PowerSourceError):
    pass

class UnsupportedSource(KP3005PError):
    pass

class NoConnectionError(KP3005PError):
    pass


class kp3005p(src.PowerSource):

    def __init__(self, comport):
        src.PowerSource.__init__(self)
        self._serial = serial.Serial(timeout=0.1)
        self._serial.port = comport
        self._devinfo = src.PowerSourceDeviceInformation()

        self._serial.baudrate = 9600
        self._serial.stopbits = 1

        self._serial.parity = 'N'
        self._serial.bytesize = 8

        self._serial.xonxoff = 0
        self._serial.rtscts  = 0

        self._lastVoltage = 0
        self._lastOutput = 0 # 0 is invalid

    def connect(self):
        if self._serial.isOpen():
            self._serial.close()

        LOG.debug("Connecting to KP3005P on port %s", self._serial.port)

        try:
            self._serial.open()
        except Exception as e:
            LOG.error("--- ERROR opening serial port: %s ---", e)
            self._serial.close()
        else:
            str = self.sendRecv("*IDN?")
            LOG.debug("*IDN: %s", str)

            if "KA3005P" in str:
                # get device information
                self.KP3005PgetDeviceInformation()

                LOG.debug(self._devinfo)

                self._lastVoltage = self.getVoltage(0)

            else:
                self._serial.close()
                raise UnsupportedSource

    def disconnect(self):
        if self._serial.isOpen():
            LOG.debug("Disconnecting from KP3005P")
            self._serial.close()
        return 0

    def getMinimalUpdateTimeMs(self):
        return 50

    def getVoltage(self, output):
        s = self.sendRecv("VOUT1?")
        try:
            f = float(s)
        except ValueError:
            f = 0.0
        return f

    def setVoltage(self, output, voltage):
        s = "VSET1:%.2f" % voltage
        if voltage >= 0:
            self._lastVoltage = voltage
        self.sendCmd(s)

    def isOutputEnabled(self, output):
        retval = False
        if output == -1:
            s = self.sendRecv("STATUS?")
            if (s[0] & 0x40)  == 0x40:
                retval = True
        return retval

    def enableOutput(self, output, value):
        self.enableOutputs(value)

    def enableOutputs(self, value):
        if value:
            self.sendCmd("OUT1")
        else:
            self.sendCmd("OUT0")

    def getDeviceInformation(self):
        return self._devinfo

    # internal functions
    def sendCmd(self, command):
        if not self._serial.isOpen():
            raise NoConnectionError
        command = command.encode()
        self._serial.write(command)

    def sendRecv(self, command):
        self.sendCmd(command)
        try:
#            s = self._serial.readline(size=None, eol='\n')
            s = self._serial.readline().decode()
        except serial.SerialTimeoutException:
            s = "Timeout waiting for device";
        return s

    def KP3005PgetDeviceInformation(self):
        s = self.sendRecv("*IDN?")

        self._devinfo.producer = s[0:5]
        self._devinfo.productName = s[5:12]
        self._devinfo.serialNo = "unknown"
        self._devinfo.softwareRevision = s[12:].strip()

        self._devinfo.currentMin = float(0)
        self._devinfo.currentMax = float(5)
        self._devinfo.currentLimitMin = float(0)
        self._devinfo.currentLimitMax = float(5)
        self._devinfo.voltageMin = float(0)
        self._devinfo.voltageMax = float(30)
        self._devinfo.voltageLimitMin = float(0)
        self._devinfo.voltageLimitMax = float(30)
        self._devinfo.voltageOVPMin = float(0)
        self._devinfo.voltageOVPMax = float(30)
        self._devinfo.outputs = 1
        self._devinfo.singleTurnOnOff = False

