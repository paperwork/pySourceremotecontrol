import serial
import sys

import pySourceremotecontrol.sources_general as src

import logging

LOG = logging.getLogger("pySourceremotecontrol.sources_hmp40x0")

class HMPError(src.PowerSourceError):
	pass

class UnsupportedBaudrateError(HMPError):
	pass

class UnsupportedStopbitsError(HMPError):
	pass

class UnsupportedHandshake(HMPError):
	pass

class UnsupportedSource(HMPError):
	pass

class NoConnectionError(HMPError):
	pass

class InvalidOutputError(HMPError):
	pass

class hmp40x0(src.PowerSource):
	HANDSHAKE_NONE   = 0
	HANDSHAKE_RTS    = 1

	def __init__(self, comport, baudrate=9600, stopbits=1, parity='N',
		    handshake=HANDSHAKE_NONE):
		src.PowerSource.__init__(self)
		self._serial = serial.Serial(timeout=1)
		self._serial.port = comport
		self._devinfo = src.PowerSourceDeviceInformation()

		supportedBaudrates = [9600, 19200, 38400, 115200]
		if not baudrate in supportedBaudrates:
			raise UnsupportedBaudrateError
		self._serial.baudrate = baudrate

		if not stopbits in [1,2]:
			raise UnsupportedStopbitsError
		self._serial.stopbits = stopbits

		self._serial.parity = parity
		self._serial.bytesize = 8
		if parity != 'N':
			self._serial.bytesize = 7

		supportedHandshake = [0,1]
		if not handshake in supportedHandshake:
			raise UnsupportedHandshake
		if handshake == hmp40x0.HANDSHAKE_NONE:
			self._serial.xonxoff = 0
			self._serial.rtscts  = 0
		else:
			self._serial.rtscts  = 1
			self._serial.xonxoff = 0

		self._lastVoltages = [0, 0, 0, 0]
		self._lastOutput = 0 # 0 is invalid


	def connect(self):
		if self._serial.isOpen():
			self._serial.close()

		LOG.debug("Connecting to HMP40x0 on port %s with baudrate %d", self._serial.port, self._serial.baudrate)

		try:
			self._serial.open()
		except Exception as e:
			LOG.error("--- ERROR opening serial port: %s ---", e)
			self._serial.close()
		else:
			str = self.HMPSendRecv("*IDN?")
			LOG.debug("*IDN: %s", str)

			if "HMP40" in str:
				self.HMPSendCmd("SYST:REM")
				self.HMPClearErrorLog()

				# get device information
				self.HMPGetDeviceInformation()

				LOG.debug(self._devinfo)

				self._lastVoltages[0] = self.getVoltage(1)
				if self._devinfo.outputs > 1:
					self._lastVoltages[1] = self.getVoltage(2)
				if self._devinfo.outputs > 2:
					self._lastVoltages[2] = self.getVoltage(3)
				if self._devinfo.outputs > 3:
					self._lastVoltages[3] = self.getVoltage(4)
			else:
				self._serial.close()
				raise UnsupportedSource


	def disconnect(self):
		if self._serial.isOpen():
			LOG.debug("Disconnecting from HMP40x0")

			self.HMPClearErrorLog()
			self.HMPSendCmd("SYST:LOC")
			self._serial.close()
		return 0


	def getMinimalUpdateTimeMs(self):
		return 50

	def getVoltage(self, output):
		self.checkLastOutput(output)
		s = self.HMPSendRecv("VOLT?")
		try:
			f = float(s)
		except ValueError:
			f = 0.0
		return f

	def setVoltage(self, output, voltage):
		self.checkLastOutput(output)
		s = "VOLT %.2f" % voltage
		if voltage > 0:
			self._lastVoltages[(output-1)] = voltage
		self.HMPSendCmd(s)

	def isOutputEnabled(self, output):
		retval = False
		if output == -1:
			s = self.HMPSendRecv("OUTP:GEN?")
			if s[0] == '1':
				retval = True
		else:
			self.checkLastOutput(output)
			s = self.HMPSendRecv("OUTP?")
			if s[0] == '1':
				retval = True
		return retval

	def enableOutput(self, output, value):
		if output == -1:
			self.enableOutputs(value)
		else:
			self.checkLastOutput(output)
			if value:
				self.HMPSendCmd("OUTP 1")
			else:
				self.HMPSendCmd("OUTP 0")

	def enableOutputs(self, value):
		if value:
			self.HMPSendCmd("OUTP:GEN 1")
		else:
			self.HMPSendCmd("OUTP:GEN 0")

	def getDeviceInformation(self):
		return self._devinfo

	# internal functions
	def checkLastOutput(self, output):
		import time
		if output > self._devinfo.outputs or output < 1:
			raise InvalidOutputError
		if self._lastOutput != output:
			self.HMPSendCmd("INST OUT" + str(output))
			self._lastOutput = output
			time.sleep(self.getMinimalUpdateTimeMs()/1000)

	def HMPSendCmd(self, command):
		if not self._serial.isOpen():
			raise NoConnectionError
		command += "\n"
		command = command.encode()
		self._serial.write(command)

	def HMPSendRecv(self, command):
		self.HMPSendCmd(command)
		try:
			s = self._serial.readline().decode()
		except serial.SerialTimeoutException:
			s = "Timeout waiting for device"
		return s

	def HMPGetLastError(self):
		if not self._serial.isOpen():
			raise NoConnectionError
		s = self.HMPSendRecv("SYST:ERR?")
		tmp = s.split(',');
		try:
			retval = int(tmp[0])
		except ValueError:
			retval = 0
		return retval

	def HMPClearErrorLog(self):
		counter = 20 # maximum error count storeable in device
		while (self.HMPGetLastError() != 0) and (counter > 0):
			counter -= 1

	def HMPGetDeviceInformation(self):
		s = self.HMPSendRecv("*IDN?")
		sList = s.split(',')

		self._devinfo.producer = sList[0].strip()
		self._devinfo.productName = sList[1].strip()
		self._devinfo.serialNo = sList[2].strip()
		self._devinfo.softwareRevision = sList[3].strip()

		s = self.HMPSendRecv("CURR? MIN")
		self._devinfo.currentMin = float(s)
		s = self.HMPSendRecv("CURR? MAX")
		self._devinfo.currentMax = float(s)
		s = self.HMPSendRecv("VOLT? MIN")
		self._devinfo.voltageMin = float(s)
		s = self.HMPSendRecv("VOLT? MAX")
		self._devinfo.voltageMax = float(s)
		s = self.HMPSendRecv("VOLT:PROT? MIN")
		self._devinfo.voltageOVPMin = float(s)
		s = self.HMPSendRecv("VOLT:PROT? MAX")
		self._devinfo.voltageOVPMax = float(s)

		if "HMP4040" in self._devinfo.productName:
			self._devinfo.outputs = 4
		else:
			self._devinfo.outputs = 3

		self._devinfo.singleTurnOnOff = True
