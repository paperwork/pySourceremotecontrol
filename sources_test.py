import sys

import pySourceremotecontrol.sources_general as src

class TestSource(src.PowerSource):
    def __init__(self):
        src.PowerSource.__init__(self)
        self._connected = False
        self._lastOutput = 0
        self._lastVoltages = [0, 0, 0, 0]
        self._outputsActive = [False, False, False, False]
        # fill out devinfo
        self._devinfo = src.PowerSourceDeviceInformation()
        self._devinfo.producer         = "Sebastian Block"
        self._devinfo.productName      = "Source Test"
        self._devinfo.serialNo         = "noSerial"
        self._devinfo.softwareRevision = "1"
        self._devinfo.outputs         =  4
        self._devinfo.currentMin      =  0.0
        self._devinfo.currentMax      = 10.0
        self._devinfo.currentLimitMin =  0.0
        self._devinfo.currentLimitMax = 10.0
        self._devinfo.voltageMin      =  0.0
        self._devinfo.voltageMax      = 60.0
        self._devinfo.voltageLimitMin =  0.0
        self._devinfo.voltageLimitMax = 60.0
        self._devinfo.voltageOVPMin   =  0.0
        self._devinfo.voltageOVPMax   = 70.0
        self._devinfo.singleTurnOnOff = True

    def connect(self):
        if self._connected:
            sys.stdout.write("TestSource: already connected\n")
        self._connected = True
        sys.stdout.write("TestSource: connected\n")

    def disconnect(self):
        self._connected = False
        sys.stdout.write("TestSource: disconnected\n")

    def getMinimalUpdateTimeMs(self):
        return 100

    def getVoltage(self, output):
        self._lastOutput = output
        return self._lastVoltages[self._lastOutput-1]
    
    def setVoltage(self, output, voltage):
        self._lastOutput = output
        if voltage > 0: 
            self._lastVoltages[self._lastOutput-1] = voltage
            sys.stdout.write("TestSource: output {0} voltage set {1:.2f}\n".format(self._lastOutput,self._lastVoltages[self._lastOutput-1]))

    def isOutputEnabled(self, output):
        if output == -1:
            retval = True
        else:
            retval = self._outputsActive[output-1]
        return retval

    def enableOutput(self, output, value):
        if output > 0:
            self._lastOutput = output
            self._outputsActive[output-1] = value
            if value:
                sys.stdout.write("TestSource: output {0} is enabled\n".format(output))
            else:
                sys.stdout.write("TestSource: output {0} is disabled\n".format(output))
    
    def enableOutputs(self, value):
        if value:
            sys.stdout.write("TestSource: enable all outputs\n")
        else:
            sys.stdout.write("TestSource: disable all outputs\n")

    def getDeviceInformation(self):
        return self._devinfo
