
class FunctionError(Exception):
    pass

class FunctionNotImplemented(FunctionError):
    pass

class Function(object):
    def __init__(self, source=None, checkCallback=None):
        self._source = source
        self._checkCallback = checkCallback

    def run(self):
        raise FunctionNotImplemented

