import unittest

from .sources_dp800 import DP800

class dp800_tests(unittest.TestCase):

	def setUp(self):
		self.dp800 = DP800()

		self.dp800.connect()

	def tearDown(self):
		self.dp800.disconnect()

	def test_set_voltage(self):
		self.dp800.setVoltage(3, 3.5)
		self.assertAlmostEqual(self.dp800.getVoltage(3), 3.5)

	def test_enable_outputs(self):
		import random
		for x in range(5):
			v = random.random() * 10
			self.dp800.setVoltage(2, v)
			self.assertAlmostEqual(self.dp800.getVoltage(2), v, places=2)
			self.dp800.enableOutputs(True)
			self.dp800.setVoltage(1, random.random() * 10)
			self.dp800.enableOutputs(False)



