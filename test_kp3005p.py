import sys

sys.path = [".."] + sys.path

import pySourceremotecontrol.function_iec61131_ramps as ir
import pySourceremotecontrol.sources_test as ts
import pySourceremotecontrol.sources_kp3005p as kp

def runCheck(checkToRun):
    retval = 0
    if checkToRun == 0:
        print("Check to be online")
    else:
        print("Check for being offline")
    return retval

t = kp.kp3005p("/dev/ttyS0")
t.connect()
t.setVoltage(24.00, 0)
ramp = ir.Ramp611312(source=t, checkCallback=runCheck, output=1, voltageStart=23, runChecks=True, count=1)
ramp.run()
t.disconnect()

